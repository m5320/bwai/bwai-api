'use strict';
const Mongoose = require('mongoose');

module.exports = class ResourceHelper {
  constructor(model, populateFields) {
    require('../models/' + model.toLowerCase());
    this.Resource = Mongoose.model(model);
    this.populateFields = populateFields || "";
  }

  async findOneByProp (prop, value) {
    return new Promise(async (resolve, reject) => {
      try {
        let resource = await this.Resource.findOne({[prop]: value})
          .populate(this.populateFields);
        return resolve(resource);
      } catch (error) {
        return reject(error);
      }
    });
  };

  async get (id) {
    return new Promise(async (resolve, reject) => {
      try {
        let resource = await this.Resource.findOne({_id: id})
          .populate(this.populateFields);;
        return resolve(resource);
      } catch (error) {
        return reject(error);
      }
    });
  };

  async createOrUpdate (data, filter) {
    return new Promise(async (resolve, reject) => {
      try {
        let c = 0, u = 0;
        for (var i = 0; i < data.length; i++) {
          const datum = data[i];

          let filteredData = {};

          if (filter) {
            for (var j = 0; j < filter.length; j++) {
              filteredData[filter[j]] = datum[filter[j]];
            }
          } else {
            filteredData = datum;
          }

          var doc = await this.Resource.findOne(filteredData);
          
          if (doc) {
            for (var p in datum) {
              doc[p] = datum[p];
            }
            await doc.save();
            u++;
          } else {
            await this.Resource.create(datum);
            c++;
          }
        } 

        return resolve({ statusCode: 200, message: 'Enregistrement réussi (' + c + ' créées, ' + u + ' mis à jour)' });
      } catch (error) {
        return reject(error);
      }
    });
  };

  async list (query = {}, page = 0, size = 15, sort = {}) {
    return new Promise(async (resolve, reject) => {
      try {
        let resources = await this.Resource.find(query)
          .populate(this.populateFields)
          .skip(page * size)
          .limit(size)
          .sort(sort)
          .lean();

        return resolve(resources);
      } catch (error) {
        return reject(error);
      }
    });
  };

  async count (query = {}) {
    return new Promise(async (resolve, reject) => {
      try {
        
        let resources = await this.Resource.count(query);

        return resolve(resources);
      } catch (error) {
        return reject(error);
      }
    });
  };

  async create (resourceData) {
    return new Promise(async (resolve, reject) => {
      try {
        let resource = new this.Resource(resourceData);
        let savedResource = await resource.save();

        return resolve({ statusCode: 200, message: 'Enregistrement réussi', resource: savedResource });
      } catch (error) {
        return reject(error);
      }
    });
  };
  
  async update (resourceId, resourceData) {
    return new Promise(async (resolve, reject) => {
      try {
        let resource = await this.Resource.findById(resourceId);

        for (var prop in resourceData) {
          resource[prop] = resourceData[prop];
        }

        let savedData = await resource.save();
        
        return resolve({
          statusCode: 200,
          resource: savedData,
          message: 'Resource saved successfully'
        });
      } catch (error) {
        reject(error);
      }
    });
  };

  async remove (resourceId) {
    return new Promise(async (resolve, reject) => {
      try {
        let removedResource = await this.Resource.remove({_id: resourceId});
        return resolve({ statusCode: 200, message: 'Resource supprimée', resource: removedResource });
      } catch (error) {
        return reject(error);
      }
    });
  };
};