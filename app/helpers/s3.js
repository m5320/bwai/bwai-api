'use strict';

const aws = require('aws-sdk');
const uuid = require('uuid');
const sharp = require('sharp');
const Config = require("../../config/config");

const s3Options = Config.get('/s3');

aws.config.update({
    region: s3Options.region,
    accessKeyId: s3Options.accessKeyId,
    secretAccessKey: s3Options.secretAccessKey
});

const s3 = new aws.S3();

exports.uploadFile = (file) => {
    console.log('in upload file s3')
    return new Promise(async function(resolve, reject) {
        const params = {
            Bucket: s3Options.bucket,
            Key: uuid.v4() + '.' + file.hapi.filename.split('.').pop(),
            Body: file._data
        };

        // Uploading files to the bucket
        s3.upload(params, function(err, data) {
            if (err) {
                reject(err);
            }
            console.log('============ upload success =============== ')
            return resolve({
                statusCode: 200,
                location: data.Location
            });
        });
    });
};

// exports.uploadImage = (file) => {
//     return new Promise(async function(resolve, reject) {
//         const params = {
//             Bucket: s3Options.bucket,
//             Key: uuid.v4() + '.' + file.hapi.filename.split('.').pop(),
//             Body: sharp(file._data).jpeg()
//         };

//         // Uploading files to the bucket
//         s3.upload(params, function(err, data) {
//             if (err) {
//                 reject(err);
//             }
//             //console.log('resulte s3:', data)
//             return resolve({
//                 statusCode: 200,
//                 location: data.Location
//             });
//         });
//     });
// };