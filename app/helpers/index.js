const UserHelper = require('./user');
const ResourceHelper = require('./resource');

module.exports = {
    admin: new UserHelper('Admin', 'email', ''),

    submission: new ResourceHelper('Submission', 'author', '-__v'),
    review: new ResourceHelper('Review', '', '-__v'),
};