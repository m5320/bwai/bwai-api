exports.sendMail = async(email, fullname, subject, text) => {
  try {
    const mailjet = require('node-mailjet').connect('269a6ded32064cf3b01b52b2e954f353', '6bbeb4f16a57bc84f69393dd4442be9b');
    const sd = mailjet
      .post("send", { 'version': 'v3.1' })
      .request({
        "Messages": [{
          "From": {
              "Email": "theontia@gmail.com ",
              "Name": "BWAI Board"
          },
          "To": [{
              "Email": email,
              "Name": fullname,
          }],
          "Subject": subject,
          "TextPart": text,
          "HTMLPart": ""
        }]
      })
    await new Promise(resolve => setTimeout(resolve, 6000));
    sd
    .then((result) => {
      console.log('Success sending ' + result.body.Messages)
      return true;
    })
    .catch((err) => {
      console.log('Error while sending ' + err)
      return err;
    })
  } catch (error) {
    console.log('error ', error)
  }
}

// "use strict";
// const nodemailer = require("nodemailer");

// // async..await is not allowed in global scope, must use a wrapper
// exports.sendMail = async(email, fullname, subject, text) => {
//   try{

//     // Generate test SMTP service account from ethereal.email
//     // Only needed if you don't have a real mail account for testing
//     let testAccount = await nodemailer.createTestAccount();

//     // create reusable transporter object using the default SMTP transport
//     let transporter = nodemailer.createTransport({
//       host: "smtp.ethereal.email",
//       port: 587,
//       secure: false, // true for 465, false for other ports
//       auth: {
//         user: testAccount.user, // generated ethereal user
//         pass: testAccount.pass, // generated ethereal password
//       },
//     });

//     // send mail with defined transport object
//     let transport = await transporter.sendMail({
//       from: '"BWAI Board 👻" <theontia@gmail.com>', // sender address
//       to: email, // list of receivers "bar@example.com, baz@example.com"
//       subject: subject, // Subject line
//       text: "", // plain text body  "Hello world?"
//       html: text, // html body
//     });

//     // var transport = nodemailer.createTransport({
//     //   host: "smtp.mailtrap.io",
//     //   port: 2525,
//     //   auth: {
//     //     user: "d00e158e4add19",
//     //     pass: "fec046df86196e"
//     //   }
//     // });
//     console.log("Message sent: %s", transport.messageId);
//     // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@example.com>

//     // Preview only available when sending through an Ethereal account
//     console.log("Preview URL: %s", nodemailer.getTestMessageUrl(transport));
//     // Preview URL: https://ethereal.email/message/WaQKMgKddxQDoou...

//   }catch (error) {console.log('error ', error)}
// }

//sendMail().catch(console.error);