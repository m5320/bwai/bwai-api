'use strict';
const MainController = require('../../controllers');

exports.plugin = {
    register: (plugin, options) => {
        const Modules = [{
              path: 'submissions/',
              ctrl: MainController.submission,
            },
            {
              path: 'reviews/',
              ctrl: MainController.review,
            },
        ];

        const basePath = '/api/';

        for (let i = 0; i < Modules.length; i++) {
            const mdl = Modules[i];
            const modulePath = basePath + mdl.path;

            plugin.route([{
                    method: 'GET',
                    path: modulePath + "{resource}",
                    config: mdl.ctrl.get()
                },
                {
                    method: 'GET',
                    path: modulePath + 'list',
                    config: mdl.ctrl.list()
                },
                {
                    method: 'GET',
                    path: modulePath + 'listNoLog',
                    config: mdl.ctrl.listNoLog()
                }, {
                    method: 'GET',
                    path: modulePath + 'getById',
                    config: mdl.ctrl.getById()
                },

                {
                    method: 'GET',
                    path: modulePath + 'count',
                    config: mdl.ctrl.count()
                },
                {
                    method: 'POST',
                    path: modulePath + 'create',
                    config: mdl.ctrl.create()
                },
                {
                    method: 'POST',
                    path: modulePath + 'createNoLog',
                    config: mdl.ctrl.createNoLog()
                },
                {
                    method: 'POST',
                    path: modulePath + 'create-file',
                    config: mdl.ctrl.createFile()
                },
                {
                    method: 'PUT',
                    path: modulePath + "{resource}/{action}",
                    config: mdl.ctrl.update()
                },
                {
                    method: 'PUT',
                    path: modulePath + "{resource}/file",
                    config: mdl.ctrl.updateWithFile()
                },
                {
                    method: 'DELETE',
                    path: modulePath + 'remove',
                    config: mdl.ctrl.remove()
                }
            ]);
        }
    },
    pkg: require('../../../package.json'),
    name: 'resource_routes_v1'
};