'use strict';
const MainController = require('../../controllers');

exports.plugin = {
    register: (plugin, options) => {
        const Modules = [{
            path: 'admins/',
            ctrl: MainController.admin,
        }];

        const basePath = '/api/';

        for (let i = 0; i < Modules.length; i++) {
            const mdl = Modules[i];
            const modulePath = basePath + mdl.path;

            plugin.route([{
                    method: 'GET',
                    path: modulePath + "{user}",
                    config: mdl.ctrl.get()
                },
                {
                    method: 'GET',
                    path: modulePath + 'me',
                    config: mdl.ctrl.getDetails()
                },
                {
                    method: 'GET',
                    path: modulePath + 'list',
                    config: mdl.ctrl.list()
                },
                {
                    method: 'GET',
                    path: modulePath + 'listNoLog',
                    config: mdl.ctrl.listNoLog()
                },
                {
                    method: 'GET',
                    path: modulePath + 'count',
                    config: mdl.ctrl.count()
                },
                {
                    method: 'POST',
                    path: modulePath + 'login',
                    config: mdl.ctrl.login()
                },
                {
                    method: 'POST',
                    path: modulePath + 'register',
                    config: mdl.ctrl.register()
                },
                {
                    method: 'POST',
                    path: modulePath + 'register-file',
                    config: mdl.ctrl.registerFile()
                },
                {
                    method: 'POST',
                    path: modulePath + 'sendCheckMail',
                    config: mdl.ctrl.sendCheckMail()
                },
                {
                    method: 'POST',
                    path: modulePath + 'sendResetPMail',
                    config: mdl.ctrl.sendResetPMail()
                },
                {
                    method: 'PUT',
                    path: modulePath + '{user}',
                    config: mdl.ctrl.update()
                },
                {
                    method: 'PUT',
                    path: modulePath + '{user}' + "/NoLog",
                    config: mdl.ctrl.updateNoLog()
                },
                {
                    method: 'DELETE',
                    path: modulePath + 'remove',
                    config: mdl.ctrl.remove()
                }
            ]);
        }

        // plugin.route([
        //      {
        //         method: 'POST',
        //         path: basePath + 'verificationmail',
        //         config: MainController.mail.verificationMail()
        //     },
        // ]);
    },
    pkg: require('../../../package.json'),
    name: 'user_routes_v1'
};