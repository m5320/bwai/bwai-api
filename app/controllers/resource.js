'use strict';
const MainHelper = require('../helpers');
const UtilHelper = require('../helpers/utils');
const S3Helper = require('../helpers/s3');
const MailSender = require('../helpers/mailsender')
const moment = require('moment')

module.exports = class ResourceController {
    constructor(type, plural, validate) {
        this.type = type;
        this.plural = plural;
        this.validate = validate;
    }

    get() {
        return {
            description: 'Returns the ' + this.type + ' info',
            auth: false,
            handler: async(request, h) => {
                try {
                    return h.response({
                        [this.type]: await MainHelper[this.type].get(request.params.resource)
                    }).code(200);
                } catch (error) {
                    return error.message;
                }
            },
            tags: ['api']
        };
    }

    getById() {
        return {
            description: 'Returns the row of ' + this.plural,
            auth: false,
            handler: async(request, h) => {
                try {
                    return h.response({
                        [this.type]: await MainHelper[this.type].get(request.payload.id)
                    }).code(200);
                } catch (error) {
                    return error.message;
                }
            },
            tags: ['api']
        };
    }

    count() {
        return {
            description: 'Returns the count of ' + this.plural,
            auth: 'jwt',
            handler: async(request, h) => {
                try {
                    if (request.query.createdAt) {
                        request.query.createdAt = JSON.parse(request.query.createdAt);
                    }

                    return h.response({
                        [this.plural]: await MainHelper[this.type].count(request.query)
                    }).code(200);
                } catch (error) {
                    return error.message;
                }
            },
            tags: ['api']
        };
    }

    list() {
        return {
            description: 'Returns the list of ' + this.total,
            auth: 'jwt',
            handler: async(request, h) => {
                try {

                    let query = request.query.options ? JSON.parse(request.query.options) : {};
                    let filter = {};
                    let sort = {};

                    if (request.query.filter) {
                        const filterData = JSON.parse(request.query.filter);
                        if (filterData.text) {
                            filter = { $or: [] };

                            filterData.type.forEach(type => {
                                filter['$or'].push({
                                    [type]: new RegExp(filterData.text, 'gi')
                                });
                            });
                        }
                    }

                    if (request.query.sort) {
                        let sortData = JSON.parse(request.query.sort);
                        for (var p in sortData) {
                            if (sortData[p] !== 0) {
                                sort[p] = sortData[p];
                            }
                        }
                    }

                    const params = {
                        ...query,
                        ...filter
                    };


                    const page = request.query.page ? parseInt(request.query.page) : 0;
                    const perPage = request.query.perPage ? parseInt(request.query.perPage) : 30;

                    let resources = await MainHelper[this.type].list(params, page, perPage, sort);
                    let resourceCount = await MainHelper[this.type].count(params);

                    // if (this.plural === 'reviews') {
                    //     // resources[0].reviewers.forEach(async(value, index) => {
                    //     //     console.log(' ')
                    //     //     console.log(index, value)
                    //     //     console.log('reviewer: =======', value.reviewer);
                    //     //     // console.log('result: ================= ', await MainHelper['admin'].get(value.reviewer));
                    //     //     resources[0].reviewers[index].reviewer = await MainHelper['admin'].get(value.reviewer);
                    //     //     console.log('finale value ========', resources[0].reviewers[index]);
                    //     //     console.log(' ')
                    //     // })

                    //     console.log('===', resources[0].reviewers, resources[0].reviewers.length)
                    //     var i = 0;
                    //     for (i = O; i < 5; i++) {
                    //         console.log(' in boucle ', i);
                    //         // console.log(i, resources[0].reviewers[i])
                    //         // console.log('reviewer: =======', resources[0].reviewers[i].reviewer);

                    //         // console.log('result: ================= ', await MainHelper['admin'].get(resources[0].reviewers[i].reviewer));
                    //         // resources[0].reviewers[i].reviewer = await MainHelper['admin'].get(vresources[0].reviewers[i].reviewer);
                    //         // console.log('finale value ========', resources[0].reviewers[i]);
                    //         // console.log(' ')  
                    //     }
                    //     let j = 10;
                    //     while (j != 0) {
                    //         console.log(' in boucle ', j);
                    //         j -= 1;
                    //     }

                    // }

                    // setTimeout(() => {
                    // }, 4000);

                    // console.log(' ----------------------------------------------- ');
                    // console.log(resources[0].reviewers);
                    // console.log(' =============================================== ')

                    return h.response({
                        [this.plural]: resources,
                        total: resourceCount
                    }).code(200);
                } catch (error) {
                    return error.message;
                }
            },
            tags: ['api']
        };
    }

    listNoLog() {
        return {
            description: 'Returns the list of ' + this.total,
            auth: false,
            handler: async(request, h) => {
                try {

                    let query = request.query.options ? JSON.parse(request.query.options) : {};
                    let filter = {};
                    let sort = {};

                    if (request.query.filter) {
                        const filterData = JSON.parse(request.query.filter);
                        if (filterData.text) {
                            filter = { $or: [] };

                            filterData.type.forEach(type => {
                                filter['$or'].push({
                                    [type]: new RegExp(filterData.text, 'gi')
                                });
                            });
                        }
                    }

                    if (request.query.sort) {
                        let sortData = JSON.parse(request.query.sort);
                        for (var p in sortData) {
                            if (sortData[p] !== 0) {
                                sort[p] = sortData[p];
                            }
                        }
                    }

                    const params = {
                        ...query,
                        ...filter
                    };

                    const page = request.query.page ? parseInt(request.query.page) : 0;
                    const perPage = request.query.perPage ? parseInt(request.query.perPage) : 30;

                    let resources = await MainHelper[this.type].list(params, page, perPage, sort);
                    let resourceCount = await MainHelper[this.type].count(params);

                    return h.response({
                        [this.plural]: resources,
                        total: resourceCount
                    }).code(200);
                } catch (error) {
                    return error.message;
                }
            },
            tags: ['api']
        };
    }

    create() {
        return {
            description: 'Create a new ' + this.type,
            auth: 'jwt',
            validate: {
                payload: this.validate.create,
                failAction: (request, h, error) => {
                    return h.response({ message: error.details[0].message.replace(/['"]+/g, '') }).code(400).takeover();
                }
            },
            handler: async(request, h) => {
                try {
                    let data = await MainHelper[this.type].create({...request.payload });

                    if (this.type == 'review') {
                        console.log('id user: ', request.payload.reviewers[0].reviewer, request.payload.deadline)
                        let deadline = moment(request.payload.deadline).format('DD-MM-YYYY');
                        let userDetail = await MainHelper['admin'].get(request.payload.reviewers[0].reviewer);
                        console.log('userDetail: ', userDetail, deadline)

                        let result = await MailSender.sendMail(userDetail.email, (userDetail.firstname + " " + userDetail.lastname), "BWAI Review",
                            "Vous avez été ajouté pour une review. Veuillez vous connecter pour les détails. La date limite de soumission de votre réponse est le " + deadline + ".");
                        console.log('result from mailjet: ', result);
                    }

                    return h.response({ message: data.message }).code(data.statusCode);
                } catch (error) {
                    return error.message;
                }
            },
            tags: ['api']
        };
    }

    createNoLog() {
        return {
            description: 'Create a new ' + this.type,
            auth: false,
            validate: {
                payload: this.validate.create,
                failAction: (request, h, error) => {
                    return h.response({ message: error.details[0].message.replace(/['"]+/g, '') }).code(400).takeover();
                }
            },
            handler: async(request, h) => {
                try {
                    let data = await MainHelper[this.type].create({...request.payload });

                    if (this.type == 'review') {
                        console.log('id user: ', request.payload.reviewers[0].reviewer, request.payload.deadline)
                        let deadline = moment(request.payload.deadline).format('DD-MM-YYYY');
                        let userDetail = await MainHelper['admin'].get(request.payload.reviewers[0].reviewer);
                        console.log('userDetail: ', userDetail, deadline)

                        let result = await MailSender.sendMail(userDetail.email, (userDetail.firstname + " " + userDetail.lastname), "BWAI Review",
                            "Vous avez été ajouté pour une review. Veuillez vous connecter pour les détails. La date limite de soumission de votre réponse est le " + deadline + ".");
                        console.log('result from mailjet: ', result);
                    }

                    return h.response({ message: data.message }).code(data.statusCode);
                } catch (error) {
                    return error.message;
                }
            },
            tags: ['api']
        };
    }

    createFile() {
        return {
            description: 'Create a new ' + this.type,
            auth: 'jwt',
            payload: {
                output: 'stream',
                allow: 'multipart/form-data',
                maxBytes: 30 * 1024 * 1024
            },
            validate: {
                payload: this.validate.createWithFile,
                failAction: (request, h, error) => {
                    return h.response({ message: error.details[0].message.replace(/['"]+/g, '') }).code(400).takeover();
                }
            },
            handler: async(request, h) => {
                try {
                    let dataRaw = JSON.parse(request.payload.objectdata);

                    dataRaw[request.payload.fileprop] = [];
                    for (var i = 0; i < parseInt(request.payload.filenumber); i++) {
                        if (request.payload['filedata' + i] && request.payload['filedata' + i] != 'null') {
                            const file = await S3Helper.uploadFile(request.payload['filedata' + i]);
                            dataRaw[request.payload.fileprop].push(file.location);
                        }
                    }
                    let data = await MainHelper[this.type].create(dataRaw);
                    //send mail to allert user
                    if (this.type == 'review') {
                        console.log('id user: ', request.payload.reviewers[(request.payload.reviewers.length - 1)].reviewer)
                        let deadline = moment(request.payload.deadline).format('DD-MM-YYYY');
                        let userDetail = await MainHelper['admin'].get(request.payload.reviewers[(request.payload.reviewers.length - 1)].reviewer);
                        console.log('userDetail: ', userDetail, deadline)

                        let result = await MailSender.sendMail(userDetail.email, (userDetail.firstname + " " + userDetail.lastname), "BWAI Review",
                            "Vous avez été ajouté pour une review. Veuillez vous connecter pour les détails. La date limite de soumission de votre réponse est le " + deadline + ".");
                        console.log('result from mailjet: ', result);
                    }

                    return h.response({ message: data.message }).code(data.statusCode);
                } catch (error) {
                    return error.message;
                }
            },
            tags: ['api']
        };
    }

    createOrUpdate() {
        return {
            description: 'Create a new set of ' + this.type,
            auth: 'jwt',
            validate: {
                payload: this.validate.createOrUpdate,
                failAction: (request, h, error) => {
                    return h.response({ message: error.details[0].message.replace(/['"]+/g, '') }).code(400).takeover();
                }
            },
            handler: async(request, h) => {
                try {
                    let data = await MainHelper[this.type].createOrUpdate(request.payload.data, request.payload.filter);
                    return h.response({ message: data.message }).code(data.statusCode);
                } catch (error) {
                    console.log(error)
                    return error.message;
                }
            },
            tags: ['api']
        };
    }

    update() {
        return {
            description: 'Update the ' + this.type,
            auth: 'jwt',
            validate: {
                //payload: this.validate.update,
                failAction: (request, h, error) => {
                    return h.response({ message: error.details[0].message.replace(/['"]+/g, '') }).code(400).takeover();
                }
            },
            handler: async(request, h) => {
                try {
                    let action = "";let globalMark = "";let reviewId = "";

                    if (request.params.action) {
                        action = request.params.action;
                        console.log('action :', request.params.action,", type: ", this.type)
                    }
                    if(this.type == 'submission' && action === 'add global mark'){
                      globalMark = request.payload.globalMark; delete request.payload['globalMark'];
                      reviewId = request.payload.reviewId; delete request.payload['reviewId']
                    }

                    let resourceId = request.params.resource;
                    let resourceData = request.payload;
                    //console.log('in update log', resourceId, resourceData)
                    let data = await MainHelper[this.type].update(resourceId, resourceData);

                    console.log('--- data ---     ', data)

                    if (this.type == 'review' && action === 'add reviewer') {
                        //console.log('---------- add reviewer ----------------------------')
                        //console.log('id user: ', request.payload.reviewers[0].reviewer, request.payload.deadline)
                        let deadline = moment(request.payload.deadline).format('DD-MM-YYYY');
                        let userDetail = await MainHelper['admin'].get(request.payload.reviewers[0].reviewer);
                        //console.log('userDetail: ', userDetail, deadline)
                        let result = await MailSender.sendMail(userDetail.email, (userDetail.firstname + " " + userDetail.lastname), "BWAI Review",
                            "Vous avez été ajouté pour une review. Veuillez vous connecter pour les détails. La date limite de soumission de votre réponse est le " + deadline + ".");
                        //console.log('result from mailjet: ', result);
                    }
                    if(this.type == 'submission' && action === 'add global mark'){
                      //console.log('---------- add global mark ----------------------------',data)
                      let reviewDetail = await MainHelper['review'].update(reviewId,{globalMark: globalMark} );
                      let userDetail = await MainHelper['admin'].get(data.resource.author);
                      //console.log('userDetail: ', userDetail, 'reviewDetail', reviewDetail)
                      let result = await MailSender.sendMail(userDetail.email, (userDetail.firstname + " " + userDetail.lastname), "BWAI Résultats",
                        "Votre soumission a été étudiée. Veuillez consulter la plateforme pour les détails de la décision finale.");
                      //console.log('result from mailjet: ', result);
                    }
                
                    // if(this.type == 'review' && action === 'make review'){
                    //   console.log('---------- make review ----------------------------')
                    //   let userDetail = await MainHelper['admin'].get(data.submission.author);
                    //   console.log('userDetail: ', userDetail)
                    //   let result = await MailSender.sendMail(userDetail.email, (userDetail.firstname + " " + userDetail.lastname), "BWAI Résultats",
                    //         "Votre soumission a été étudiée. <br> Veuillez consulter la plateforme pour les détails de la décision finale.");
                    //   console.log('result from mailjet: ', result);
                    // }
                    //console.log(' =-=-=-=-=-=-=-=-=-=-finale response', data.resource)
                    return h.response({ message: 'Done', [this.type]: data.resource }).code(200);
                } catch (error) {
                    return error.message;
                }
            },
            tags: ['api']
        };
    }


    updateWithFile() {
        return {
            description: 'Update with file a new ' + this.type,
            auth: 'jwt',
            payload: {
                output: 'stream',
                allow: 'multipart/form-data' // important
            },
            validate: {
                payload: this.validate.createWithFile,
                failAction: (request, h, error) => {
                    return h.response({ message: error.details[0].message.replace(/['"]+/g, '') }).code(400).takeover();
                }
            },
            handler: async(request, h) => {
                try {
                    let resourceId = request.params.resource;
                    let dataRaw = JSON.parse(request.payload.objectdata);
                    let fileLinks = JSON.parse(request.payload.filelinks);

                    dataRaw[request.payload.fileprop] = [];

                    for (var i = 0; i < parseInt(request.payload.filenumber); i++) {
                        if (request.payload['filedata' + i] && request.payload['filedata' + i] != 'null') {
                            const file = await S3Helper.uploadImage(request.payload['filedata' + i]);
                            dataRaw[request.payload.fileprop].push(file.location);
                        } else {
                            dataRaw[request.payload.fileprop].push(fileLinks[i]);
                        }
                    }

                    console.log(dataRaw);

                    let data = await MainHelper[this.type].update(resourceId, dataRaw);
                    return h.response({ message: data.message }).code(data.statusCode);
                } catch (error) {
                    return error.message;
                }
            },
            tags: ['api']
        };
    }

    remove() {
        return {
            description: 'Remove the ' + this.type,
            auth: 'jwt',
            validate: {
                payload: this.validate.remove,
                failAction: (request, h, error) => {
                    return h.response({ message: error.details[0].message.replace(/['"]+/g, '') }).code(400).takeover();
                }
            },
            handler: async(request) => {
                try {
                    return await MainHelper[this.type].remove(request.payload.id);
                } catch (error) {
                    return error.message;
                }
            },
            tags: ['api']
        };
    }

    import () {
        return {
            description: 'Create a new ' + this.type,
            auth: 'jwt',
            payload: {
                output: 'stream',
                allow: 'multipart/form-data' // important
            },
            validate: {
                payload: this.validate.import,
                failAction: (request, h, error) => {
                    return h.response({ message: error.details[0].message.replace(/['"]+/g, '') }).code(400).takeover();
                }
            },
            handler: async(request, h) => {
                try {
                    let data = UtilHelper.parseExcel(request.payload.file._data);
                    const values = await this.validate.importData(data, request.payload.ecole);
                    // console.log(values);
                    // return 'yo';
                    return await MainHelper[this.type].createOrUpdate(values);
                } catch (error) {
                    console.error(error);
                    return error.message;
                }
            },
            tags: ['api']
        };
    }

};