'use strict';
var JWT = require('jsonwebtoken');
const { mail } = require('.');
const Config = require('../../config/config');
const MainHelper = require('../helpers');
const S3Helper = require('../helpers/s3');
const MailSender = require('../helpers/mailsender')

module.exports = class UserController {
    constructor(type, plural, validate) {
        this.type = type;
        this.plural = plural;
        this.validate = validate;
    }

    getDetails() {
        return {
            description: 'Returns the ' + this.type + ' info',
            auth: 'jwt',
            handler: async(request, h) => {
                try {
                    return h.response({
                        [this.type]: await MainHelper[this.type].findUserDetails(request.headers.userId)
                    }).code(200);
                } catch (error) {
                    return error.message;
                }
            },
            tags: ['api']
        };
    }

    get() {
        return {
            description: 'Returns the ' + this.type + ' info',
            auth: 'jwt',
            handler: async(request, h) => {
                try {
                    return h.response({
                        [this.type]: await MainHelper[this.type].get(request.params.user)
                    }).code(200);
                } catch (error) {
                    return error.message;
                }
            },
            tags: ['api']
        };
    }

    count() {
        return {
            description: 'Returns the count of ' + this.plural,
            auth: 'jwt',
            handler: async(request, h) => {
                try {
                    if (request.query.createdAt) {
                        request.query.createdAt = JSON.parse(request.query.createdAt);
                    }

                    return h.response({
                        [this.plural]: await MainHelper[this.type].count(request.query)
                    }).code(200);
                } catch (error) {
                    return error.message;
                }
            },
            tags: ['api']
        };
    }

    list() {
        return {
            description: 'Returns the list of ' + this.total,
            auth: 'jwt',
            handler: async(request, h) => {
                try {

                    let query = request.query.options ? JSON.parse(request.query.options) : {};
                    let filter = {};
                    let sort = {};

                    if (request.query.filter) {
                        const filterData = JSON.parse(request.query.filter);
                        if (filterData.text) {
                            filter = { $or: [] };

                            filterData.type.forEach(type => {
                                filter['$or'].push({
                                    [type]: new RegExp(filterData.text, 'gi')
                                });
                            });
                        }
                    }

                    if (request.query.sort) {
                        let sortData = JSON.parse(request.query.sort);
                        for (var p in sortData) {
                            if (sortData[p] !== 0) {
                                sort[p] = sortData[p];
                            }
                        }
                    }

                    const params = {
                        ...query,
                        ...filter
                    };

                    const page = request.query.page ? parseInt(request.query.page) : 0;
                    const perPage = request.query.perPage ? parseInt(request.query.perPage) : 1000;

                    let users = await MainHelper[this.type].list(params, page, perPage, sort);
                    let userCount = await MainHelper[this.type].count(params);

                    return h.response({
                        [this.plural]: users,
                        total: userCount
                    }).code(200);
                } catch (error) {
                    return error.message;
                }
            },
            tags: ['api']
        };
    }

    listNoLog() {
        return {
            description: 'Returns the list of ' + this.total,
            auth: false,
            handler: async(request, h) => {
                try {

                    let query = request.query.options ? JSON.parse(request.query.options) : {};
                    let filter = {};
                    let sort = {};

                    if (request.query.filter) {
                        const filterData = JSON.parse(request.query.filter);
                        if (filterData.text) {
                            filter = { $or: [] };

                            filterData.type.forEach(type => {
                                filter['$or'].push({
                                    [type]: new RegExp(filterData.text, 'gi')
                                });
                            });
                        }
                    }

                    if (request.query.sort) {
                        let sortData = JSON.parse(request.query.sort);
                        for (var p in sortData) {
                            if (sortData[p] !== 0) {
                                sort[p] = sortData[p];
                            }
                        }
                    }

                    const params = {
                        ...query,
                        ...filter
                    };

                    const page = request.query.page ? parseInt(request.query.page) : 0;
                    const perPage = request.query.perPage ? parseInt(request.query.perPage) : 1000;

                    let users = await MainHelper[this.type].list(params, page, perPage, sort);
                    let userCount = await MainHelper[this.type].count(params);

                    return h.response({
                        [this.plural]: users,
                        total: userCount
                    }).code(200);
                } catch (error) {
                    return error.message;
                }
            },
            tags: ['api']
        };
    }

    register() {
        return {
            description: 'Create a new ' + this.type,
            auth: false,
            validate: {
                payload: this.validate.register,
                failAction: (request, h, error) => {
                    return h.response({ message: error.details[0].message.replace(/['"]+/g, '') }).code(400).takeover();
                }
            },
            handler: async(request, h) => {
                try {
                    let data = await MainHelper[this.type].register({...request.payload });
                    //send mail to allert user
                    if (data.user != null) {
                        let result = await MailSender.sendMail(request.payload.email, (request.payload.firstname + " " + request.payload.lastname), "BWAI Bienvenue",
                            "Félicitations ! !! Bienvenue sur le Hub BWAI. </br> Profitez de votre exploration du monde de l'intelligence artificielle et contribuez à sa construction.");
                        console.log('result from mailjet: ', result);
                    }
                    return h.response({ message: data.message, user: data.user }).code(data.statusCode);
                } catch (error) {
                    return error.message;
                }
            },
            tags: ['api']
        };
    }

    registerFile() {
        return {
            description: 'Create a new ' + this.type,
            auth: false,
            payload: {
                output: 'stream',
                allow: 'multipart/form-data', // important,
                maxBytes: 100 * 1024 * 1024
            },
            validate: {
                payload: this.validate.registerFile,
                failAction: (request, h, error) => {
                    console.log(' ------ error in register file: ')
                    return h.response({ message: error.details[0].message.replace(/['"]+/g, '') }).code(400).takeover();
                }
            },
            handler: async(request, h) => {
                console.log(' ')
                console.log('in register with file')
                try {
                    let dataRaw = JSON.parse(request.payload.objectdata);
                    dataRaw[request.payload.fileprop] = [];
                    for (var i = 0; i < parseInt(Number(request.payload.filenumber)); i++) {
                        if (request.payload['filedata' + i] && request.payload['filedata' + i] != 'null') {
                            console.log('new upload: ', request.payload['filedata' + i])
                            const file = await S3Helper.uploadFile(request.payload['filedata' + i]);
                            dataRaw[request.payload.fileprop].push(file.location);
                        }
                    }
                    console.log(' ');

                    let data = await MainHelper[this.type].register(dataRaw);
                    console.log(data);

                    if (data.user != null) {
                        let result = await MailSender.sendMail(data.user.email, (data.user.firstname + " " + data.user.lastname), "BWAI Bienvenue",
                            "Félicitations ! !! Bienvenue sur le Hub BWAI.</br> Profitez de votre exploration du monde de l'intelligence artificielle et contribuez à sa construction.");
                        console.log('result from mailjet: ', result);
                    }
                    return h.response({ message: data.message, statut: 200, user: data.user }).code(data.statusCode);
                } catch (error) {
                    return error.message;
                }
            },
            tags: ['api']
        };
    }

    // async sendWelcomMail(email, fullname) {
    //     try {
    //         const mailjet = require('node-mailjet').connect('269a6ded32064cf3b01b52b2e954f353',
    //             '6bbeb4f16a57bc84f69393dd4442be9b')
    //         const sd = mailjet
    //             .post("send", { 'version': 'v3.1' })
    //             .request({
    //                 "Messages": [{
    //                     "From": {
    //                         "Email": "theontia@gmail.com ",
    //                         "Name": "BWAI Board"
    //                     },
    //                     "To": [{
    //                         "Email": email,
    //                         "Name": fullname,
    //                     }],
    //                     "Subject": "Welcome on BWAI Hub !",
    //                     "TextPart": "Congratulations !!! Welcome on the BWAI Hub. Enjoy your artificial intelligence world exploration and contribuate to his building.",
    //                     "HTMLPart": ""
    //                 }]
    //             })

    //         await new Promise(resolve => setTimeout(resolve, 6000));
    //         sd
    //             .then((result) => {
    //                 console.log('Success sending ' + result.body.Messages)
    //                 return true;
    //             })
    //             .catch((err) => {
    //                 console.log('Error while sending ' + err)
    //                 return err;
    //             })

    //     } catch (error) {
    //         console.log('error ', error)
    //     }
    // }

    sendCheckMail() {
        return {
            auth: false,
            handler: async(request, h) => {
                try {
                    try {
                        const mailjet = require('node-mailjet').connect('269a6ded32064cf3b01b52b2e954f353',
                            '6bbeb4f16a57bc84f69393dd4442be9b')
                        const sd = mailjet
                            .post("send", { 'version': 'v3.1' })
                            .request({
                                "Messages": [{
                                    "From": {
                                        "Email": "theontia@gmail.com ",
                                        "Name": "Ejima Board"
                                    },
                                    "To": [{
                                        "Email": request.payload.email, //"anagoarmandine@gmail.com"
                                        "Name": request.payload.fullname, //"passenger 1"
                                    }],
                                    "Subject": "Account Validation !",
                                    "TextPart": "You are almost there, you still have to validate your email address. Your validation code : " + request.payload.code + " .",
                                    "HTMLPart": ""
                                }]
                            })

                        await new Promise(resolve => setTimeout(resolve, 6000));
                        sd
                            .then((result) => {
                                console.log('Success sending ' + result.body.Messages)
                                return h.response({
                                    message: "success",
                                }).code(200);
                            })
                            .catch((err) => {
                                console.log('Error while sending ' + err)
                                return h.response({
                                    message: "error",
                                }).code(200);
                            })

                    } catch (error) {
                        console.log('error ', error)
                    }

                    return h.response({
                        message: "no result",
                    }).code(200);
                } catch (error) {
                    return error;
                }
            },
            tags: ['api']
        };
    }

    sendResetPMail() {
        return {
            auth: false,
            handler: async(request, h) => {
                try {
                    let resultMail = " ";
                    console.log(' ')
                    console.log('Notre email :', request.payload.email);
                    let users = await MainHelper['admin'].list({ email: request.payload.email });

                    if (users === null) {
                        return h.response({
                            message: "email not found",
                        }).code(400);
                    }

                    console.log('user :', users[0]);
                    let fullname = users[0].firstname + " " + users[0].lastname;
                    let email = request.payload.email;
                    let code = users[0].code;

                    console.log('bof :', code, fullname, email);
                    console.log(' ')

                    try {
                        const mailjet = require('node-mailjet').connect('269a6ded32064cf3b01b52b2e954f353',
                            '6bbeb4f16a57bc84f69393dd4442be9b')
                        const sd = mailjet
                            .post("send", { 'version': 'v3.1' })
                            .request({
                                "Messages": [{
                                    "From": {
                                        "Email": "theontia@gmail.com ",
                                        "Name": "Ejima Board"
                                    },
                                    "To": [{
                                        "Email": email,
                                        "Name": fullname,
                                    }],
                                    "Subject": "Reset Password !",
                                    "TextPart": "Your validation code : " + code + " to reset your password .",
                                    "HTMLPart": ""
                                }]
                            })

                        await new Promise(resolve => setTimeout(resolve, 6000));
                        sd
                            .then((result) => {
                                console.log('Success sending ' + result.body.Messages)
                                return h.response({
                                    message: "success",
                                }).code(200);
                            })
                            .catch((err) => {
                                console.log('Error while sending ' + err)
                                return h.response({
                                    message: "error",
                                }).code(400);
                            })

                    } catch (error) {
                        console.log('error ', error)
                    }

                    return h.response({
                        message: "success",
                        id: users[0]._id,
                        code: code
                    }).code(200);
                } catch (error) {
                    return error;
                }
            },
            tags: ['api']
        };
    }

    login() {
        return {
            description: 'Login to your account',
            auth: false,
            validate: {
                payload: this.validate.login,
                failAction: (request, h, error) => {
                    return h.response({ message: error.details[0].message.replace(/['"]+/g, '') }).code(400).takeover();
                }
            },
            handler: async(request, h) => {
                try {
                    let data = await MainHelper[this.type].findByCredentials(request.payload.email, request.payload.password);

                    if (data.statusCode === 200) {
                        let secret = Config.get('/jwtAuthOptions/key');

                        let obj = {
                            userId: data.user.id
                        };

                        let jwtToken = JWT.sign(obj, secret, { expiresIn: '7 days' });

                        data.user.password = undefined;
                        data.user.salt = undefined;

                        var response = h.response({ message: 'Successfully login', [this.type]: data.user, token: jwtToken });

                        response.header('Authorization', jwtToken);
                        response.code(200);

                        return response;
                    } else {
                        return h.response({ message: data.message }).code(data.statusCode);
                    }
                } catch (error) {
                    return error.message;
                }
            },
            tags: ['api']
        };
    }

    update() {
        return {
            description: 'Remove the ' + this.type,
            auth: 'jwt',
            validate: {
                payload: this.validate.update,
                failAction: (request, h, error) => {
                    return h.response({ message: error.details[0].message.replace(/['"]+/g, '') }).code(400).takeover();
                }
            },
            handler: async(request, h) => {
                try {
                    let userId = request.params.user;
                    let userData = request.payload;

                    let data = await MainHelper[this.type].update(userId, userData);

                    return h.response({ message: 'Done', [this.type]: data.user }).code(200);
                } catch (error) {
                    return error.message;
                }
            },
            tags: ['api']
        };
    }


    updateNoLog() {
        return {
            description: 'Remove the ' + this.type,
            auth: false,
            validate: {
                payload: this.validate.update,
                failAction: (request, h, error) => {
                    return h.response({ message: error.details[0].message.replace(/['"]+/g, '') }).code(400).takeover();
                }
            },
            handler: async(request, h) => {
                try {
                    let userId = request.params.user;
                    let userData = request.payload;

                    let data = await MainHelper[this.type].update(userId, userData);

                    return h.response({ message: 'Done', [this.type]: data.user }).code(200);
                } catch (error) {
                    return error.message;
                }
            },
            tags: ['api']
        };
    }

    remove() {
        return {
            description: 'Remove the ' + this.type,
            auth: 'jwt',
            validate: {
                payload: this.validate.remove,
                failAction: (request, h, error) => {
                    return h.response({ message: error.details[0].message.replace(/['"]+/g, '') }).code(400).takeover();
                }
            },
            handler: async(request) => {
                try {
                    return await MainHelper[this.type].remove(request.payload.id);
                } catch (error) {
                    return error.message;
                }
            },
            tags: ['api']
        };
    }
};