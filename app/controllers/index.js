const UserController = require('./user');
const ResourceController = require('./resource');
const validation = require('../validations');

module.exports = {
    admin: new UserController('admin', 'admins', validation.admin),

    submission: new ResourceController('submission', 'submissions', validation.submission),
    review: new ResourceController('review', 'reviews', validation.review),
};