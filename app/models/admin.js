'use strict';

const Mongoose = require('mongoose');
const Schema = Mongoose.Schema;
const crypto = require('crypto');
var short = require('short-uuid');

/**
 * Admin Schema
 */
var AdminSchema = new Schema({
    email: { type: String, unique: true },
    password: { type: String },
    firstname: { type: String },
    lastname: { type: String },
    code: { type: String },
    pictures: [{ type: String }],
    active: { type: Boolean, default: true },
    profession: { type: String },
    role: {
        type: String,
        enum: ['admin', 'reviewer', 'user'],
        default: 'user'
    },
}, {
    timestamps: true,
    toObject: {
        virtuals: true
    },
    toJSON: {
        virtuals: true
    }
});

/**
 * Hook a pre save method to hash the password
 */
AdminSchema.pre('save', function(next) {
    if (this.password && this.isModified('password')) {
        this.password = this.hashPassword(this.password);
    }
    next();
});

/**
 * Hook a pre update method to hash the password
 */
AdminSchema.pre('update', function(next) {
    if (this.password && this.isModified('password')) {
        this.password = this.hashPassword(this.password);
    }
    next();
});

/**
 * Create instance method for hashing a password
 */
AdminSchema.methods.hashPassword = function(password) {
    var shaSum = crypto.createHash('sha256');
    shaSum.update(password);
    return shaSum.digest('hex');
};

/**
 * Create instance method for authenticating Admin
 */
AdminSchema.methods.authenticate = function(password) {
    return this.password === this.hashPassword(password);
};

AdminSchema.pre('save', function(next) {
    if (this._id == (null || undefined)) {
        next();
    } else {
        this.code = short.generate().slice(0, 7);
        console.log('Our code : ', this.code);
        next();
    }
});

Mongoose.model('Admin', AdminSchema);