//pictures: [{ type: String }],
// parrain: { type: Mongoose.Schema.Types.ObjectId, ref: 'Admin' },

'use strict';

const Mongoose = require('mongoose');
const Schema = Mongoose.Schema;

/**
 * Submission Schema
 */
var SubmissionSchema = new Schema({
    files: [{ type: String }],
    date: { type: Date },
    title: { type: String },
    summary: { type: String },
    status: { type: Boolean, default: false },
    reviewingComments: { type: String },

    author: { type: Mongoose.Schema.Types.ObjectId, ref: 'Admin' },
}, {
    timestamps: true,
    toObject: {
        virtuals: true
    },
    toJSON: {
        virtuals: true
    }
});

Mongoose.model('Submission', SubmissionSchema);