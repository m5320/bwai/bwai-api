//pictures: [{ type: String }],
// parrain: { type: Mongoose.Schema.Types.ObjectId, ref: 'Admin' },

'use strict';

const Mongoose = require('mongoose');
const Schema = Mongoose.Schema;

/**
 * Review Schema
 */
var ReviewSchema = new Schema({
    globalMark: { type: String },
    deadline: { type: Date },
    reviewers: [{
        reviewer: { type: Mongoose.Schema.Types.ObjectId, ref: 'Admin' },
        mark: { type: String },
        summary: { type: String },
        comment: { type: String },
    }],
    submission: { type: Mongoose.Schema.Types.ObjectId, ref: 'Submission' },
}, {
    timestamps: true,
    toObject: {
        virtuals: true
    },
    toJSON: {
        virtuals: true
    }
});

Mongoose.model('Review', ReviewSchema);