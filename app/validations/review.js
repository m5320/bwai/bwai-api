const Joi = require('joi');

module.exports = {
    create: {
        reviewers: Joi.array().required(),
        deadline: Joi.date().required(),
        submission: Joi.string().required(),
    },
    update: {
        globalMark: Joi.string().optional(),
        deadline: Joi.date().optional(),
        reviewers: Joi.array().optional(),
        submission: Joi.string().optional(),
    },
    remove: {
        id: Joi.string().required()
    },
}