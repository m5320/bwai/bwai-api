const Joi = require('joi');

module.exports = {
    create: {
        reviewingComments: Joi.string().optional(),

        date: Joi.date().required(),
        summary: Joi.string().required(),
        author: Joi.string().required(),
    },
    createWithFile: {
        fileprop: Joi.any().optional(),
        filenumber: Joi.any().optional(),
        objectdata: Joi.any().optional(),
        filedata0: Joi.any().optional(),
    },
    update: {
        status: Joi.boolean().optional(),
        reviewingComments: Joi.string().optional(),
        date: Joi.date().optional(),
        summary: Joi.string().optional(),
        author: Joi.string().optional(),
    },
    remove: {
        id: Joi.string().required()
    },
}