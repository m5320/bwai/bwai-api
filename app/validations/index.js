const admin = require('./admin');

const submission = require('./submission');
const review = require('./review');



module.exports = {
    admin,

    submission,
    review,
};