const Joi = require('joi');

module.exports = {
    register: {
        role: Joi.string().optional(),
        active: Joi.boolean().optional(),

        email: Joi.string().required(),
        password: Joi.string().required(),
        firstname: Joi.string().required(),
        lastname: Joi.string().required(),
        profession: Joi.string().required(),
    },
    registerFile: {
        fileprop: Joi.any().optional(),
        filenumber: Joi.any().optional(),
        objectdata: Joi.any().optional(),
        filedata0: Joi.any().optional(),
    },
    login: {
        email: Joi.string().required(),
        password: Joi.string().required(),
    },
    update: {
        email: Joi.string().optional(),
        firstname: Joi.string().optional(),
        lastname: Joi.string().optional(),
        role: Joi.string().optional(),
        active: Joi.boolean().optional(),
        profession: Joi.string().optional(),
    },
    remove: {
        id: Joi.string().required()
    }
}